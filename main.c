#include "avr/interrupt.h"
#include "avr/sleep.h"
#include "functions.h"



int main(void)
{
	
	/* Setup interrupts */
	DDRD &= ~((1 << PIND2) | (1 << PIND3) );
	PORTD |= (1 << PIND2) | (1 << PIND3) ;
	EICRA = (1 << ISC10) | (1 << ISC00); // Both interrupts with any level change
	EIMSK = 0b11;
	
	/* Setup output */
	DDRB |= (1 << PINB0) | (1 << PINB1) | (1 << PINB2) | (1 << PINB3);
	if (!(PIND & (1 << PIND3)))
	{
		supply_on();
	}
	else supply_off();
	
	/* Power management */
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable();
	sei();
	
	
	
	
	while(1)
	{
		sleep_cpu();
	}
	
}

ISR(INT0_vect)
{
	cli();
	_delay_ms(3000);
	if (PIND & (1 << PIND3))
	{
		if ( !(PIND & (1 << PIND2)))
		{
			supply_on();
		}
		else if ( PIND & (1 << PIND2)) 
		{
			supply_off();
		}
	}
	else 
		if (PIND & (1 << PIND2))
		{
			voice_signal(5);
		}
		

	EIFR = (1 << INTF1) | (1 << INTF0);
	sei();
}
ISR(INT1_vect)
{
	cli();
	_delay_ms(3000);
	if ( !(PIND & (1 << PIND3)))
	{
		voice_signal(5);
		supply_on();
		
	}
	else 
	{
		if (!(PIND & (1 << PIND2)))
		{
			supply_on();
		}
		else supply_off();
	}
	EIFR = (1 << INTF1) | (1 << INTF0);
	sei();
}