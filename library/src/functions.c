#include "functions.h"

void supply_on()
{
	PORTB &= ~((1 << PANEL));
	_delay_ms(500);
	PORTB &= ~((1 << SYS_ACU) | (1 << CAR_ACU));
	_delay_ms(500);
	PORTB |= (1 << SYS_ACU);
}
void supply_off()
{
	PORTB &= ~((1 << SYS_ACU) | (1 << CAR_ACU));
	_delay_ms(500);
	PORTB |= (1 << CAR_ACU);
	_delay_ms(500);
	PORTB |= (1 << PANEL);
}
void voice_signal(int len)
{
	PORTB ^= (1 << SPEAKER);
	_delay_ms(500);
	for (int i = 0; i < len * 2; i ++)
	{
		PORTB ^= (1 << SPEAKER);
		_delay_ms(150);
	}
	PORTB &= ~(1 << SPEAKER);
}