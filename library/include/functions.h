#include "avr/io.h"
#define  F_CPU 16000000UL
#include "avr/delay.h"

#define SPEAKER PINB3
#define CAR_ACU PINB0
#define SYS_ACU PINB1
#define PANEL	PINB2
/************************************************************************/
/* @brief	Function changing system supply to car engine. It provides that
/*			no both supplies will be on. Because of used driver, first we
/*			must disconnect solar panel.
/************************************************************************/
void supply_on();
/************************************************************************/
/* @brief	Function changing system supply to Solar system. It provides 
/*			that no both supplies will be on.
/************************************************************************/
void supply_off();
/************************************************************************/
/* @brief	Signal from buzzer with generator. 3sec sound then *len* single
/*			beeps
/* @param[in] amount of beeps after longer sound
/************************************************************************/
void voice_signal(int len);